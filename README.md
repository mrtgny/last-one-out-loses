# Installation

```
yarn
```

> A faster way

```
yarn install --frozen-lockfile
```

> OR

```
npm install
```

# Run

## Development

```
yarn dev
```

> OR

```
npm run dev
```

# Getting started

## Part 1

- In `_app.tsx` pushes to route `choose_opponent` page initially to force the user select an opponent.
- `PlayBoard` component manages the `Coins`. `Remove` and `Select` actions are implemented inside of it.
- `GameContext` manages the `scores` and `opponent type`. `PlayBoard` component calls the `addMove` function to show the previos moves in `ScoreBoard` component.
- `PlayBoard` watches the `Coins`. If there is no coin, it updates the `gameStatus` and pushes to `game_over` page.
- `game_over` page also gets the `winner` from `GameContext`.
- `Computer` is a component in `PlayGround`. So, it can access the `GameContext` and also edit the `PlayBoard`. It tries to win but it playes offensive!
- `Game Over` page shows also the latest `ScoreBoard`. Also, it has the `Play Again` button to restart the game.

## Part 2

- `_app.tsx` loads the `choose_opponent` page initially.
- `GameContext` manages the status of the game and keeps the score history. `PlayBoard` notifies the context when a user makes a move.
- `ScoreBoard` reads tthe history from the context.
- When the game is over it pushes to `GameOver` page.
- It shows the latest `ScoreBoard` and also allows the users to restart the game.

## Part 3

- I centered the `PlayGround` and the other items because I think that would be easier to watch and play the game.
- I added a `ScoreBoard` for the both users to allow to see them the history.
- Set the background to black because I thought by that way it doesn't tire the eyes and looks simple.
- I designed the ui simple as possible to avoid a complicated interface.

## Part 4

I couldn't calculate it but it seems there is an advantage for the first player. The first player can control the game by removing certain amount of coin for each turn. For example, when the second player removes 1 coin, the first player can remove 2 to make the total an odd number. If the second player removes 2 coins then the first player removes 1 or 3. So, there may be an advantage by going first.

# Why?

- I used the `PlayBoard` component to manage the map instead of `GameContext`. Because I thought the `coin array` doesn't need to be in the context. It just notifies the context to add the removed coins count to the history.
- Also I choose to use accessing to context instead of passing the functions and variables as props to the components like `PlayBoard` or `ScoreBoard`. Because I already created a context to make it accessable at anywhere already.
- I used `enum` for the player types. Because there is limited and certain amount of types.

# Would Do In More Time

- Animation for removing the selected coins
- High score logic.
- Storing the last match in local storage, so the user can continue later.
- A timer
