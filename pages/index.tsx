import PlayBoard from "components/PlayBoard";
import ScoreBoard from "components/ScoreBoard";
import { NextPage } from "next/types";

const Home: NextPage = () => {
  return (
    <div>
      <div className="flex justify-center items-center">
        <div className="flex flex-col items-center text-5xl">
          <PlayBoard />
        </div>
      </div>
      <ScoreBoard />
    </div>
  );
};

export default Home;
