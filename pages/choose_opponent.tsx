import Button from "components/Button";
import { useGame } from "context/hook";
import { useRouter } from "next/router";
import { NextPage } from "next/types";
import { OPPONENT_TYPE } from "utils/types";

const ChooseOpponent: NextPage = () => {
  const { chooseOpponent } = useGame();
  const { push } = useRouter();
  const onSelect = (opponent: OPPONENT_TYPE) => {
    chooseOpponent(opponent);
    push("/");
  };

  return (
    <div>
      <div className="flex justify-center items-center">
        <div className="flex flex-col items-center text-5xl">
          <h1 className="text-white my-20 underline">Choose Opponent</h1>
          <Button
            enabledClassName="border-blue-400 text-blue-400"
            onClick={() => onSelect(OPPONENT_TYPE.PLAYER)}
          >
            Player
          </Button>
          <Button
            enabledClassName="border-red-400 text-red-400"
            onClick={() => onSelect(OPPONENT_TYPE.COMPUTER)}
          >
            Computer
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ChooseOpponent;
