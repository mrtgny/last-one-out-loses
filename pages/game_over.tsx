import Button from "components/Button";
import ScoreBoard from "components/ScoreBoard";
import { useGame } from "context/hook";
import { useRouter } from "next/router";
import { NextPage } from "next/types";
import { getPlayerTitle } from "utils/functions";
import { OPPONENT_TYPE, PLAYERS } from "utils/types";

const ChooseOpponent: NextPage = () => {
  const { push } = useRouter();
  const { winner, opponentType, restartGame } = useGame();
  const isOpponentComputer = opponentType === OPPONENT_TYPE.COMPUTER;
  const isWinnerComputer = isOpponentComputer && winner === PLAYERS.PLAYER2;
  const playAgain = () => {
    restartGame();
    push("/choose_opponent");
  };

  return (
    <div className="flex justify-center w-full">
      <div className="max-w-xl w-full">
        <div className="flex flex-col items-center text-5xl w-full">
          <h1 className="text-2xl mb-4">Congratulations!</h1>
          <div className="text-4xl mb-4">
            {getPlayerTitle(winner!)} {isWinnerComputer ? "(Computer)" : null}{" "}
            Wins!
          </div>
          <div className="px-4 w-full">
            <Button
              enabledClassName="border-blue-400 text-blue-400"
              onClick={playAgain}
            >
              Play Again!
            </Button>
          </div>
          <div className="text-base w-full">
            <ScoreBoard />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ChooseOpponent;
