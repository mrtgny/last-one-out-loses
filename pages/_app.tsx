import Layout from "components/Layout";
import GameProvider from "context/provider";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { useEffect } from "react";
import "../styles/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  const { push } = useRouter();

  useEffect(() => {
    // Force the user choose an opponent
    push("/choose_opponent");
    // Disabled to prevent call the effect on every route changes
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <GameProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </GameProvider>
    </>
  );
}

export default MyApp;
