import Coin from "components/Coin";
import { useGame } from "context/hook";
import { FC, Fragment } from "react";
import { OPPONENT_TYPE, PLAYERS } from "utils/types";

const ScoreBoard = () => {
  const { opponentType } = useGame();
  const isOpponentComputer = opponentType === OPPONENT_TYPE.COMPUTER;
  return (
    <div className="grid grid-cols-2 m-4 gap-4">
      <div className="col-span-1 border-[1px] border-blue-400 p-4">
        <h3 className="text-blue-400 mb-4">Player 1</h3>
        <ScoreTable player={PLAYERS.PLAYER1} />
      </div>
      <div className="col-span-1 border-[1px] border-red-400 p-4">
        <h3 className="text-red-400 mb-4">
          Player 2 {isOpponentComputer ? "(Computer)" : null}
        </h3>
        <ScoreTable player={PLAYERS.PLAYER2} />
      </div>
    </div>
  );
};

const ScoreTable: FC<{ player: PLAYERS }> = ({ player }) => {
  const { moves } = useGame();
  const playerMoves = moves[player];
  return (
    <div className="grid grid-cols-8 gap-2">
      {playerMoves.map((count, index) => {
        return (
          <Fragment key={index}>
            <div className="col-span-1">{index + 1}.</div>
            <div className="col-span-7 flex items-center">
              <div className="flex gap-1">
                {Array(count)
                  .fill(1)
                  .map((_, index) => {
                    return <Coin mini key={index} />;
                  })}
              </div>
            </div>
          </Fragment>
        );
      })}
    </div>
  );
};

export default ScoreBoard;
