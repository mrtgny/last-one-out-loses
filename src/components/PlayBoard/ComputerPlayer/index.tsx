import { useGame } from "context/hook";
import { FC, useEffect } from "react";
import { COINT_STATUS, GAME_STATUS, PLAYERS } from "utils/types";

interface IComputerPlayerProps {
  playMap: number[][];
  removeCoins: (coins: number[][]) => void;
}

const ComputerPlayer: FC<IComputerPlayerProps> = ({ playMap, removeCoins }) => {
  const { currentTurn, gameStatus, addMove } = useGame();

  const isComputersTurn = currentTurn === PLAYERS.PLAYER2;
  const isGameInProgress = gameStatus === GAME_STATUS.IN_PROGRESS;

  useEffect(() => {
    if (isComputersTurn && isGameInProgress) {
      const possibleMoves: number[][] = [];
      playMap.forEach((column, i) => {
        column.forEach((row, t) => {
          if (row === COINT_STATUS.DEFAULT) possibleMoves.push([i, t]);
        });
      });

      const timer = setTimeout(() => {
        // if there is more coins it removes them by 3 to finish the game faster.
        if (possibleMoves.length > 6) {
          removeCoins(possibleMoves.slice(0, 3));
          // if there is between 2-4 coins, it removes one less coins to win. If there is 1, it loses.
        } else if (possibleMoves.length <= 4) {
          removeCoins(
            possibleMoves.slice(0, Math.max(possibleMoves.length - 1, 1))
          );
          // It removes only one coin to get a chance to win.
        } else {
          removeCoins(possibleMoves.slice(0, 1));
        }
        // It waits for a second to make the move visible.
      }, 1000);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [isComputersTurn, isGameInProgress, playMap, removeCoins, addMove]);

  return null;
};

export default ComputerPlayer;
