import classNames from "classnames";
import Button from "components/Button";
import Coin from "components/Coin";
import { useGame } from "context/hook";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { getPlayerTitle } from "utils/functions";
import { COINT_STATUS, GAME_STATUS, OPPONENT_TYPE, PLAYERS } from "utils/types";
import ComputerPlayer from "./ComputerPlayer";

const getInitialPlayMap = () =>
  Array(5).fill(Array(4).fill(COINT_STATUS.DEFAULT));

const PlayBoard = () => {
  const { push } = useRouter();
  const { addMove, opponentType, currentTurn, updateGameStatus } = useGame();
  const [playMap, setPlayMap] = useState<number[][]>(getInitialPlayMap());
  const [selected, setSelected] = useState<number[][]>([]);

  const getIsSelected = (column: number, row: number) => {
    return selected.filter(([c, r]) => c === column && r === row).length > 0;
  };

  useEffect(() => {
    let isFinished = true;
    playMap.forEach((column) => {
      column.forEach((row) => {
        if (row === COINT_STATUS.DEFAULT) {
          isFinished = false;
          return;
        }
      });
    });
    if (isFinished) {
      updateGameStatus(GAME_STATUS.GAME_OVER);
      push("/game_over");
    }
    // Disabled to prevent call the effect on every route changes
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playMap, updateGameStatus]);

  const selectCoin = (column: number, row: number) => {
    setSelected((old) => {
      let selectedIndex = -1;
      const newArray = [...old.map((i) => [...i])];
      for (let index = 0; index < newArray.length; index++) {
        const [_column, _row] = newArray[index];
        if (_column === column && _row === row) {
          selectedIndex = index;
          break;
        }
      }
      if (selectedIndex > -1) {
        newArray.splice(selectedIndex, 1);
      } else {
        if (old.length === 3) return old;
        newArray.push([column, row]);
      }

      return newArray;
    });
  };

  const removeSelectedCoins = () => {
    removeCoins(selected);
  };

  const removeCoins = useCallback(
    (coins: number[][]) => {
      addMove(coins.length);
      setPlayMap((old) => {
        const newArray = [...old.map((i) => [...i])];
        coins.forEach(([column, row]) => {
          newArray[column][row] = COINT_STATUS.REMOVED;
        });
        return newArray;
      });
      setSelected([]);
    },
    [addMove]
  );

  const isOpponentComputer = opponentType === OPPONENT_TYPE.COMPUTER;
  const isComputersTurn = isOpponentComputer && currentTurn === PLAYERS.PLAYER2;
  const disabled = selected.length === 0 || isComputersTurn;

  const selectionDisabled = isComputersTurn;

  return (
    <>
      {isOpponentComputer ? (
        <ComputerPlayer playMap={playMap} removeCoins={removeCoins} />
      ) : null}
      <div>
        <h1 className="text-white mb-8">
          Turn: {getPlayerTitle(currentTurn as unknown as PLAYERS)}
        </h1>
        <div className="border-2 border-white">
          <div className="grid grid-cols-5">
            {playMap.map((row, i) => {
              return row.map((value, t) => {
                const isHidden = value === COINT_STATUS.REMOVED;
                const isSelected = getIsSelected(i, t);
                return (
                  <div
                    key={t}
                    onClick={() =>
                      !isHidden && !selectionDisabled && selectCoin(i, t)
                    }
                    className={classNames(
                      "border-[1px] border-white flex justify-center items-center p-8",
                      {
                        "cursor-not-allowed": selectionDisabled,
                        "bg-yellow-400/20": isSelected,
                      }
                    )}
                  >
                    <Coin isHidden={isHidden} />
                  </div>
                );
              });
            })}
          </div>
        </div>
        <Button
          onClick={removeSelectedCoins}
          enabledClassName="border-yellow-400 text-yellow-400"
          disabled={disabled}
        >
          Apply
        </Button>
      </div>
    </>
  );
};

export default PlayBoard;
