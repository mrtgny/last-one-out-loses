import classNames from "classnames";
import { FC } from "react";

interface ICoinProps {
  isHidden?: boolean;
  mini?: boolean;
}

const Coin: FC<ICoinProps> = ({ isHidden, mini = false }) => {
  return (
    <div
      className={classNames(
        "col-span-1 aspect-square bg-yellow-400 rounded-full text-sm",
        {
          "w-12 h-12": !mini,
          "w-6 h-6": mini,
          "opacity-0": isHidden,
        }
      )}
    />
  );
};

export default Coin;
