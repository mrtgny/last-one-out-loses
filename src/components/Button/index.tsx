import classNames from "classnames";
import { ButtonHTMLAttributes, DetailedHTMLProps, FC } from "react";

interface IButtonProps {
  enabledClassName?: string;
}

const Button: FC<
  DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > &
    IButtonProps
> = ({ onClick, className, disabled, enabledClassName, children }) => {
  return (
    <button
      onClick={onClick}
      className={classNames(
        `w-full mt-4 border-2 p-4 text-center text-2xl transition-opacity block ${className}`,
        {
          [`${enabledClassName} hover:opacity-60 cursor-pointer`]: !disabled,
          "border-gray-400 text-gray-400 hover:bg-gray-400/40 cursor-not-allowed":
            disabled,
        }
      )}
    >
      {children}
    </button>
  );
};

export default Button;
