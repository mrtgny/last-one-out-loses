import { createContext } from "react";
import { IGameContext } from "./types";

export const GameContext = createContext({} as IGameContext);
