import { GameContext } from "context";
import { FC, PropsWithChildren, useCallback, useEffect, useState } from "react";
import { GAME_STATUS, IMoves, OPPONENT_TYPE, PLAYERS } from "utils/types";

const getInitialMoves = () => ({ player1: [], player2: [] });

const GameProvider: FC<PropsWithChildren> = ({ children }) => {
  const [moves, setMoves] = useState<IMoves>(getInitialMoves());
  const [currentTurn, setCurrentTurn] = useState<PLAYERS | undefined>(
    undefined
  );
  const [opponentType, setOpponentType] = useState<OPPONENT_TYPE | undefined>(
    undefined
  );
  const [gameStatus, setGameStatus] = useState(GAME_STATUS.IDLE);
  const [winner, setWinner] = useState<PLAYERS | undefined>(undefined);

  const addMove = useCallback(
    (count: number) => {
      setMoves((old) => {
        if (currentTurn === undefined) return old;
        old[currentTurn].push(count);
        return { ...old };
      });
      setCurrentTurn((old) => {
        if (old === PLAYERS.PLAYER1) return PLAYERS.PLAYER2;
        return PLAYERS.PLAYER1;
      });
    },
    [currentTurn]
  );

  const chooseOpponent = (opponent: OPPONENT_TYPE) => {
    const randomTurn = Math.random() * 2;
    setCurrentTurn(randomTurn > 1 ? PLAYERS.PLAYER2 : PLAYERS.PLAYER1);
    setOpponentType(opponent);
    setGameStatus(GAME_STATUS.IN_PROGRESS);
  };

  const updateGameStatus = useCallback(
    (status: GAME_STATUS) => {
      if (status === GAME_STATUS.GAME_OVER) {
        setWinner(currentTurn);
      }
      setGameStatus(status);
    },
    [currentTurn]
  );

  const restartGame = () => {
    setMoves(getInitialMoves());
    setCurrentTurn(undefined);
    setOpponentType(undefined);
    setGameStatus(GAME_STATUS.IDLE);
    setWinner(undefined);
  };

  useEffect(() => {}, [gameStatus, currentTurn]);

  return (
    <GameContext.Provider
      value={{
        restartGame,
        winner,
        chooseOpponent,
        updateGameStatus,
        gameStatus,
        currentTurn,
        addMove,
        opponentType,
        moves,
      }}
    >
      {children}
    </GameContext.Provider>
  );
};

export default GameProvider;
