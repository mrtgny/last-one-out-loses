import { GAME_STATUS, IMoves, OPPONENT_TYPE, PLAYERS } from "utils/types";

export interface IGameContext {
  addMove: (count: number) => void;
  moves: IMoves;
  restartGame: () => void;
  chooseOpponent: (opponent: OPPONENT_TYPE) => void;
  updateGameStatus: (status: GAME_STATUS) => void;
  currentTurn?: PLAYERS;
  winner?: PLAYERS;
  opponentType?: OPPONENT_TYPE;
  gameStatus: GAME_STATUS;
}
