import { GameContext } from "context";
import { useContext } from "react";

export const useGame = () => useContext(GameContext);
