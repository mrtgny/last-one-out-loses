export enum OPPONENT_TYPE {
  PLAYER = "player",
  COMPUTER = "computer",
}

export enum PLAYERS {
  PLAYER1 = "player1",
  PLAYER2 = "player2",
}

export enum GAME_STATUS {
  IDLE = "idle",
  IN_PROGRESS = "in_progress",
  GAME_OVER = "game_over",
}

export enum COINT_STATUS {
  REMOVED = 0,
  DEFAULT = 1,
}

export interface IMoves extends Record<PLAYERS, number[]> {}
