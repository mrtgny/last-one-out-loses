import { PLAYERS } from "./types";

export const getPlayerTitle = (player: PLAYERS) => {
  if (player === PLAYERS.PLAYER1) return "Player 1";
  return "Player 2";
};
